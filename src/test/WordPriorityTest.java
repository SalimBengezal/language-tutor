import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.dsr.javaschool.tutor.TestConfig;
import ru.dsr.javaschool.tutor.entity.Practice;
import ru.dsr.javaschool.tutor.entity.Word;
import ru.dsr.javaschool.tutor.entity.WordPractice;
import ru.dsr.javaschool.tutor.repository.PracticeRepository;
import ru.dsr.javaschool.tutor.repository.WordRepository;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class WordPriorityTest {

    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private PracticeRepository practiceRepository;

    @Before
    public void Init() {
        Word w1 = new Word("привет", "hello");
        Word w2 = new Word("мир", "world");
        Word w3 = new Word("пока", "goodbye");
        Word w4 = new Word("дверь", "door");
        Word w5 = new Word("ноутбук", "notebook");

        Practice p1 = new Practice();
        addWordToPractice(w1, p1, true);
        addWordToPractice(w2, p1, true);
        addWordToPractice(w3, p1, false);
        addWordToPractice(w4, p1, false);

        Practice p2 = new Practice();
        addWordToPractice(w2, p2, true);
        addWordToPractice(w4, p2, true);

        Practice p3 = new Practice();
        addWordToPractice(w2, p3, true);
        addWordToPractice(w3, p3, true);
        addWordToPractice(w4, p3, false);

        Practice p4 = new Practice();
        addWordToPractice(w2, p4, true);

        practiceRepository.save(p1);
        practiceRepository.save(p2);
        practiceRepository.save(p3);
        practiceRepository.save(p4);
        wordRepository.save(w1);
        wordRepository.save(w2);
        wordRepository.save(w3);
        wordRepository.save(w4);
        wordRepository.save(w5);
    }

    private void addWordToPractice(Word w, Practice p, boolean isCorrect) {
        WordPractice wordPractice = new WordPractice();
        wordPractice.setCorrect(isCorrect);
        wordPractice.setWord(w);
        wordPractice.setPractice(p);
        w.getWordPractices().add(wordPractice);
    }

    @Test
    public void PriorityTest1() {
        List<Word> words = wordRepository.getExerciseWords(1);
        Assert.assertTrue(words.get(0).getEn().equals("notebook"));
        Assert.assertTrue(words.get(0).getRu().equals("ноутбук"));
    }


    @Test
    public void PriorityTest2() {
        List<Word> words = wordRepository.getExerciseWords(2);
        Assert.assertTrue(words.get(0).getEn().equals("notebook"));
        Assert.assertTrue(words.get(0).getRu().equals("ноутбук"));
        Assert.assertTrue(words.get(1).getEn().equals("hello"));
        Assert.assertTrue(words.get(1).getRu().equals("привет"));

    }

    @Test
    public void PriorityTest3() {
        List<Word> words = wordRepository.getExerciseWords(3);
        Assert.assertTrue(words.get(0).getEn().equals("notebook"));
        Assert.assertTrue(words.get(0).getRu().equals("ноутбук"));
        Assert.assertTrue(words.get(1).getEn().equals("hello"));
        Assert.assertTrue(words.get(1).getRu().equals("привет"));
        Assert.assertTrue(words.get(2).getEn().equals("goodbye"));
        Assert.assertTrue(words.get(2).getRu().equals("пока"));
    }

    @Test
    public void PriorityTest4() {
        List<Word> words = wordRepository.getExerciseWords(4);
        Assert.assertTrue(words.get(0).getEn().equals("notebook"));
        Assert.assertTrue(words.get(0).getRu().equals("ноутбук"));
        Assert.assertTrue(words.get(1).getEn().equals("hello"));
        Assert.assertTrue(words.get(1).getRu().equals("привет"));
        Assert.assertTrue(words.get(2).getEn().equals("goodbye"));
        Assert.assertTrue(words.get(2).getRu().equals("пока"));
        Assert.assertTrue(words.get(3).getEn().equals("door"));
        Assert.assertTrue(words.get(3).getRu().equals("дверь"));

    }

    @Test
    public void PriorityTest5() {
        List<Word> words = wordRepository.getExerciseWords(5);
        Assert.assertTrue(words.get(0).getEn().equals("notebook"));
        Assert.assertTrue(words.get(0).getRu().equals("ноутбук"));
        Assert.assertTrue(words.get(1).getEn().equals("hello"));
        Assert.assertTrue(words.get(1).getRu().equals("привет"));
        Assert.assertTrue(words.get(2).getEn().equals("goodbye"));
        Assert.assertTrue(words.get(2).getRu().equals("пока"));
        Assert.assertTrue(words.get(3).getEn().equals("door"));
        Assert.assertTrue(words.get(3).getRu().equals("дверь"));
        Assert.assertTrue(words.get(4).getEn().equals("world"));
        Assert.assertTrue(words.get(4).getRu().equals("мир"));

    }

}
