import org.junit.Assert;
import org.junit.Test;
import ru.dsr.javaschool.tutor.translate.WordParser;

import java.util.List;

public class WordParserTest {

    @Test
    public void containsTrueNumberOfWords(){
        List<String> wordList = WordParser.getWords("look for a picture");
        Assert.assertEquals(wordList.size(), 4);
    }

    @Test
    public void containsVerbWithPreposition(){
        List<String> wordList = WordParser.getWords("look for a picture");
        Assert.assertEquals(wordList.size(), 4);
        Assert.assertTrue(wordList.contains("look for"));
    }

    @Test
    public void notContainArticleA(){
        List<String> wordList = WordParser.getWords("look for a picture");
        Assert.assertEquals(wordList.size(), 4);
        Assert.assertFalse(wordList.contains("a"));
    }

    @Test
    public void notContainArticleAn(){
        List<String> wordList = WordParser.getWords("an apple");
        Assert.assertEquals(wordList.size(), 1);
        Assert.assertFalse(wordList.contains("an"));
    }

    @Test
    public void notContainArticleThe(){
        List<String> wordList = WordParser.getWords("this is the picture");
        Assert.assertEquals(wordList.size(), 3);
        Assert.assertFalse(wordList.contains("the"));
    }

    @Test
    public void wordsWithSpacesAndSymbols(){
        List<String> wordList = WordParser.getWords("this,    is !?   my  .  house");
        Assert.assertEquals(wordList.size(), 4);
        Assert.assertTrue(wordList.contains("this"));
        Assert.assertTrue(wordList.contains("is"));
        Assert.assertTrue(wordList.contains("my"));
        Assert.assertTrue(wordList.contains("house"));
    }

    @Test
    public void wordWithUpperAndLowerCase(){
        List<String> wordList = WordParser.getWords("tHis Is mY HOUSE");
        Assert.assertEquals(wordList.size(), 4);
        Assert.assertTrue(wordList.contains("this"));
        Assert.assertTrue(wordList.contains("is"));
        Assert.assertTrue(wordList.contains("my"));
        Assert.assertTrue(wordList.contains("house"));
    }

    @Test
    public void RussianAndEnglishWords(){
        List<String> wordList = WordParser.getWords("father папа");
        Assert.assertEquals(wordList.size(), 2);
        Assert.assertTrue(wordList.contains("father"));
        Assert.assertTrue(wordList.contains("папа"));
    }

}
