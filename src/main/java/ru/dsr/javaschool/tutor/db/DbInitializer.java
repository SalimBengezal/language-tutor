package ru.dsr.javaschool.tutor.db;

import org.springframework.beans.factory.annotation.Autowired;
import ru.dsr.javaschool.tutor.entity.Practice;
import ru.dsr.javaschool.tutor.entity.Word;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.dsr.javaschool.tutor.entity.WordPractice;
import ru.dsr.javaschool.tutor.repository.PracticeRepository;
import ru.dsr.javaschool.tutor.repository.WordRepository;

import javax.annotation.PostConstruct;

@Component
public class DbInitializer {

    @Autowired
    WordRepository wordRepository;

    @Autowired
    PracticeRepository practiceRepository;

    @PostConstruct
    @Transactional
    private void Init() {
        Word w1 = new Word("привет", "hello");
        Word w2 = new Word("мир", "world");
        Word w3 = new Word("пока", "goodbye");
        Word w4 = new Word("дверь", "door");
        Word w5 = new Word("ноутбук", "notebook");


        Practice p1 = new Practice();
        addWordToPractice(w1, p1, true);
        addWordToPractice(w2, p1, true);
        addWordToPractice(w3, p1, false);
        addWordToPractice(w4, p1, false);

        Practice p2 = new Practice();
        addWordToPractice(w2, p2, true);
        addWordToPractice(w4, p2, true);
        addWordToPractice(w5, p2, false);

        Practice p3 = new Practice();
        addWordToPractice(w2, p3, true);
        addWordToPractice(w3, p3, true);
        addWordToPractice(w4, p3, false);
        addWordToPractice(w5, p3, false);

        practiceRepository.save(p1);
        practiceRepository.save(p2);
        practiceRepository.save(p3);
        wordRepository.save(w1);
        wordRepository.save(w2);
        wordRepository.save(w3);
        wordRepository.save(w4);
        wordRepository.save(w5);

    }


    private void addWordToPractice(Word w, Practice p, boolean isCorrect) {
        WordPractice wordPractice = new WordPractice();
        wordPractice.setCorrect(isCorrect);
        wordPractice.setPractice(p);
        wordPractice.setWord(w);
        w.getWordPractices().add(wordPractice);
    }

}
