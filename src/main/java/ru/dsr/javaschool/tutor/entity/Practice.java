package ru.dsr.javaschool.tutor.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Practice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "practice")
    private Set<WordPractice> wordPractices;


    public Practice() {
        wordPractices = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<WordPractice> getWordPractices() {
        return wordPractices;
    }

    public void setWordPractices(Set<WordPractice> wordPractices) {
        this.wordPractices = wordPractices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Practice practice = (Practice) o;

        return id.equals(practice.id) && wordPractices.equals(practice.wordPractices);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + wordPractices.hashCode();
        return result;
    }
}
