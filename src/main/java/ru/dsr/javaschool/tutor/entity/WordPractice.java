package ru.dsr.javaschool.tutor.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class WordPractice implements Serializable{

    private boolean isCorrect;
    private Practice practice;
    private Word word;

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "practice_id")
    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "word_id")
    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

}