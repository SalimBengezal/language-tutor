package ru.dsr.javaschool.tutor.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String ru;

    @Column(unique = true)
    private String en;

    @JsonIgnore
    @OneToMany(mappedBy = "word",cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<WordPractice> wordPractices;

    public Word(String ru, String en) {
        this.ru = ru;
        this.en = en;
        wordPractices = new HashSet<>();
    }

    public Word() {
        wordPractices = new HashSet<>();
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRu() {
        return ru;
    }

    public void setRu(String ru) {
        this.ru = ru;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public Set<WordPractice> getWordPractices() {
        return wordPractices;
    }

    public void setWordPractices(Set<WordPractice> wordPractices) {
        this.wordPractices = wordPractices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (!id.equals(word.id)) return false;
        if (!ru.equals(word.ru)) return false;
        return en.equals(word.en) && (wordPractices != null ? wordPractices.equals(word.wordPractices) : word.wordPractices == null);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + ru.hashCode();
        result = 31 * result + en.hashCode();
        result = 31 * result + (wordPractices != null ? wordPractices.hashCode() : 0);
        return result;
    }
}
