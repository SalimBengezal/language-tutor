package ru.dsr.javaschool.tutor.validation.annotations;

import ru.dsr.javaschool.tutor.validation.validators.WordEnValidator;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = WordEnValidator.class)
public @interface English {
    String message();

    Class[] groups() default {};

    Class[] payload() default {};

}
