package ru.dsr.javaschool.tutor.validation.dto;

public class ValidationErrorDTO {

    public ValidationErrorDTO() {
    }

    public ValidationErrorDTO(String fieldName, String msg) {
        this.fieldName = fieldName;
        this.msg = msg;
    }

    private String fieldName;
    private String msg;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
