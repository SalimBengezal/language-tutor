package ru.dsr.javaschool.tutor.validation.annotations;

import ru.dsr.javaschool.tutor.validation.validators.WordRuValidator;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = WordRuValidator.class)
public @interface Russian {

    String message();

    Class[] groups() default {};

    Class[] payload() default {};
}
