package ru.dsr.javaschool.tutor.validation.dto;

import org.hibernate.validator.constraints.NotBlank;
import ru.dsr.javaschool.tutor.validation.annotations.English;
import ru.dsr.javaschool.tutor.validation.annotations.Russian;

import javax.validation.constraints.NotNull;

public class WordDTO {


    @NotNull
    @NotBlank(message = "Слово не должно быть пустым")
    @English(message = "Слово должно содержать латинские буквы")
    private String en;

    @NotNull
    @Russian(message = "Слово должно содержать латинские буквы")
    @NotBlank(message = "Слово не должно быть пустым")
    private String ru;

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getRu() {
        return ru;
    }

    public void setRu(String ru) {
        this.ru = ru;
    }
}
