package ru.dsr.javaschool.tutor.validation.validators;

import ru.dsr.javaschool.tutor.validation.annotations.English;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WordEnValidator implements ConstraintValidator<English, String> {
   public void initialize(English constraint) {
   }

   public boolean isValid(String text, ConstraintValidatorContext context) {
      return text.matches("[a-zA-Z]+(\\s[a-zA-Z]+)?");
   }
}
