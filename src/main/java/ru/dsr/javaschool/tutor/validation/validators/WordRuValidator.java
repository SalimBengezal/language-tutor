package ru.dsr.javaschool.tutor.validation.validators;

import ru.dsr.javaschool.tutor.validation.annotations.Russian;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WordRuValidator implements ConstraintValidator<Russian, String> {
    public void initialize(Russian constraint) {
    }

    public boolean isValid(String text, ConstraintValidatorContext context) {
        return text.matches("[а-яА-Я]+(\\s[а-яА-Я]+)?");
    }
}
