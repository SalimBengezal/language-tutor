package ru.dsr.javaschool.tutor.handlers;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.dsr.javaschool.tutor.validation.dto.ValidationErrorDTO;
import ru.dsr.javaschool.tutor.validation.exceptions.ValidationException;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public String exceptionHandler(Exception e,
                                   ModelMap modelMap) {
        modelMap.addAttribute("error", e.getMessage());
        return "error";
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(ValidationException.class)
    @ResponseBody
    @ResponseStatus(BAD_REQUEST)
    public List<ValidationErrorDTO> validationErrorHandler(ValidationException e) {

        return e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map((x) -> new ValidationErrorDTO(x.getField(), x.getDefaultMessage()))
                .collect(toList());

    }
}
