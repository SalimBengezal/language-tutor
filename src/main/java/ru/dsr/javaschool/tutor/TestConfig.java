package ru.dsr.javaschool.tutor;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import ru.dsr.javaschool.tutor.db.DbInitializer;

@EnableAutoConfiguration
@ComponentScan(value = "ru.dsr.javaschool.tutor", excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = DbInitializer.class)})
public class TestConfig {

}
