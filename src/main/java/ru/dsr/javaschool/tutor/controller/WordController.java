package ru.dsr.javaschool.tutor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.dsr.javaschool.tutor.entity.Word;
import ru.dsr.javaschool.tutor.service.WordService;
import ru.dsr.javaschool.tutor.validation.dto.ListDTO;
import ru.dsr.javaschool.tutor.validation.dto.WordDTO;
import ru.dsr.javaschool.tutor.validation.exceptions.ValidationException;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/word")
public class WordController {

    @Autowired
    private WordService wordService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> getAll() {
        return wordService.getAll();
    }

    @RequestMapping(value = "/get/en-ru", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> getAllRuWords() {
        return wordService.getAllEnWords();
    }

    @RequestMapping(value = "/get/ru-en", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> getAllEnWords() {
        return wordService.getAllRuWords();
    }

    @RequestMapping(value = "/en-ru/translate", method = RequestMethod.POST)
    @ResponseBody
    public List<Word> translateEnglishWords(@RequestBody String text) throws Exception {
        return wordService.translateWordsFromEnglish(text);
    }

    @RequestMapping(value = "/ru-en/translate", method = RequestMethod.POST)
    @ResponseBody
    public List<Word> translateRussianWords(@RequestBody String text) throws Exception {
        return wordService.translateWordsFromRussian(text);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Word addWord(@RequestBody @Valid WordDTO word, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            throw new ValidationException("Validation Error", bindingResult);
        return wordService.save(word);
    }

    @RequestMapping(value = "/add/list", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(NO_CONTENT)
    public void addWord(@RequestBody @Valid ListDTO<WordDTO> words, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            throw new ValidationException("Validation Error", bindingResult);
        wordService.saveAll(words);
    }


}
