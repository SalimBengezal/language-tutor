package ru.dsr.javaschool.tutor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WebController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index() {
        return "pages/index";
    }

    @RequestMapping(value = "/add/word", method = RequestMethod.GET)
    public String addWord() {
        return "pages/addWord";
    }

    @RequestMapping(value = "/add/text", method = RequestMethod.GET)
    public String addText() {
        return "pages/translateText";
    }

    @RequestMapping(value = "/practice/statistics", method = RequestMethod.GET)
    public String getPracticeStatistics() {
        return "pages/practiceStatistics";
    }

    @RequestMapping(value = "/words", method = RequestMethod.GET)
    public String getWords() {
        return "pages/listWord";
    }

    @RequestMapping(value = "/practice/quickstart", method = RequestMethod.GET)
    public String quickStartPractice() {
        return "pages/practiceQuickStart";
    }

    @RequestMapping(value = "/practice/choice", method = RequestMethod.GET)
    public String quickStartOwnListPractice() {
        return "pages/practiceOwnListStart";
    }
}
