package ru.dsr.javaschool.tutor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.dsr.javaschool.tutor.entity.Practice;
import ru.dsr.javaschool.tutor.entity.Word;
import ru.dsr.javaschool.tutor.service.PracticeService;
import ru.dsr.javaschool.tutor.service.WordService;
import ru.dsr.javaschool.tutor.validation.dto.WordDTO;

import java.util.List;

@RestController
@RequestMapping("/api/practice")
public class PracticeController {

    private static final int NUMBER_OF_EXERCISES = 4;

    @Autowired
    PracticeService practiceService;

    @Autowired
    WordService wordService;

    @RequestMapping("/statistics")
    @ResponseBody
    public List<Practice> getPractices() {
        return practiceService.getAll();
    }

    @RequestMapping(value = "/quickstart/en-ru", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> getExerciseWordsENRU() {
        return wordService.getWordsForExerciseENRU(NUMBER_OF_EXERCISES);
    }

    @RequestMapping(value = "/quickstart/ru-en", method = RequestMethod.GET)
    @ResponseBody
    public List<Word> getExerciseWordsRUEN() {
        return wordService.getWordsForExerciseRUEN(NUMBER_OF_EXERCISES);
    }

    @RequestMapping(value = "/check/en-ru", method = RequestMethod.POST)
    @ResponseBody
    public Practice testPassingENRU(@RequestBody List<WordDTO> words) {
        return practiceService.addPractice(words, "en-ru");
    }

    @RequestMapping(value = "/check/ru-en", method = RequestMethod.POST)
    @ResponseBody
    public Practice testPassingRUEN(@RequestBody List<WordDTO> words) {
        return practiceService.addPractice(words, "ru-en");
    }

}
