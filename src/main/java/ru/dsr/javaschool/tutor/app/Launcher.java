package ru.dsr.javaschool.tutor.app;

import org.springframework.boot.SpringApplication;
import ru.dsr.javaschool.tutor.AppConfig;

public class Launcher {
    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class);
    }
}
