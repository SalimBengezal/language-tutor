package ru.dsr.javaschool.tutor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dsr.javaschool.tutor.entity.Word;
import ru.dsr.javaschool.tutor.repository.WordRepository;
import ru.dsr.javaschool.tutor.translate.WordParser;
import ru.dsr.javaschool.tutor.validation.dto.ListDTO;
import ru.dsr.javaschool.tutor.validation.dto.WordDTO;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WordService {

    @Autowired
    private WordRepository wordRepository;

    @Autowired
    private TranslateService translateService;

    @Transactional
    public List<Word> getAll() {
        return (List<Word>) wordRepository.findAll();
    }

    @Transactional
    public List<Word> getAllRuWords() {
        List<Word> words = (List<Word>) wordRepository.findAll();
        return words.stream().map(word -> new Word(word.getRu(), "")).collect(Collectors.toList());
    }

    @Transactional
    public List<Word> getAllEnWords() {
        List<Word> words = (List<Word>) wordRepository.findAll();
        return words.stream().map(word -> new Word("", word.getEn())).collect(Collectors.toList());
    }

    @Transactional
    public Word findByEn(String name) {
        return wordRepository.findByEn(name);
    }

    @Transactional
    public Word findByRu(String name) {
        return wordRepository.findByRu(name);
    }

    @Transactional
    public Word save(WordDTO word) {
        Word w = new Word(word.getRu(), word.getEn());
        return wordRepository.save(w);
    }

    @Transactional
    public void saveAll(ListDTO<WordDTO> wordsDTO) {
        for (WordDTO word : wordsDTO.getList()) {
            wordRepository.save(new Word(word.getRu(), word.getEn()));
        }
    }

    public List<Word> translateWordsFromEnglish(String text) throws Exception {
        List<String> wordsParserList = WordParser.getWords(text);
        List<Word> wordsList = new LinkedList<>();
        for (String word : wordsParserList) {
            if (word.matches("[a-zA-Z]+(\\s[a-zA-Z]+)?"))
                wordsList.add(new Word(translateService.translateToRussian(word), word));
        }
        return wordsList;
    }

    public List<Word> translateWordsFromRussian(String text) throws Exception {
        List<String> wordsParserList = WordParser.getWords(text);
        List<Word> wordsList = new LinkedList<>();
        for (String word : wordsParserList) {
            if (word.matches("[а-яА-Я]+(\\s[а-яА-Я]+)?"))
                wordsList.add(new Word(word, translateService.translateToEnglish(word)));
        }
        return wordsList;
    }

    public List<Word> getWordsForExerciseRUEN(Integer limit) {
        List<Word> words = wordRepository.getExerciseWords(limit);
        for (Word word :
                words) {
            word.setWordPractices(null);
            word.setEn("");
        }
        return words;
    }

    public List<Word> getWordsForExerciseENRU(Integer limit) {
        List<Word> words = wordRepository.getExerciseWords(limit);
        for (Word word :
                words) {
            word.setWordPractices(null);
            word.setRu("");
        }
        return words;
    }


}
