package ru.dsr.javaschool.tutor.service;

import com.google.gson.Gson;
import org.springframework.stereotype.Service;
import ru.dsr.javaschool.tutor.translate.YandexResponse;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

@Service
class TranslateService {

    private URL getUrlWithParams(String lang, String text) throws MalformedURLException {
        String API_KEY = "trnsl.1.1.20161209T092846Z.f33f6a1753b86f2f.d7c62eaacba09694a499c9382223a44b1cc599f0";
        String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?";
        return new URL(URL + "key=" + API_KEY + "&lang=" + lang + "&text=" + text);
    }

    private YandexResponse executeRequest(URL url) throws Exception {
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");
        connection.setRequestProperty("Accept-Charset", "UTF-8");
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        InputStream inputStream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String str;
        while (null != (str = reader.readLine()))
            builder.append(str);

        if (responseCode != 200) {
            throw new Exception("Error from YANDEX API");
        }
        String result = builder.toString();
        return new Gson().fromJson(result, YandexResponse.class);
    }


    String translateToRussian(String word) throws Exception {
        YandexResponse yandexResponse = executeRequest(getUrlWithParams("en-ru", word));
        return yandexResponse.getText()[0];
    }

    String translateToEnglish(String word) throws Exception {
        YandexResponse yandexResponse = executeRequest(getUrlWithParams("ru-en", word));
        return yandexResponse.getText()[0];
    }
}
