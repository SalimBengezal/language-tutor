package ru.dsr.javaschool.tutor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dsr.javaschool.tutor.validation.dto.WordDTO;
import ru.dsr.javaschool.tutor.entity.Practice;
import ru.dsr.javaschool.tutor.entity.Word;
import ru.dsr.javaschool.tutor.entity.WordPractice;
import ru.dsr.javaschool.tutor.repository.PracticeRepository;
import ru.dsr.javaschool.tutor.repository.WordRepository;

import java.util.*;

@Service
public class PracticeService {

    @Autowired
    PracticeRepository practiceRepository;

    @Autowired
    WordRepository wordRepository;

    public List<Practice> getAll() {
        List<Practice> practices = (List<Practice>) practiceRepository.findAll();
        List<Practice> newPractices = new ArrayList<>();
        for (Practice practice : practices) {
            Practice p = new Practice();
            p.setId(practice.getId());
            Set<WordPractice> wordPractices = new HashSet<>();
            for (WordPractice wordPractice : practice.getWordPractices()) {
                WordPractice wp = new WordPractice();
                wp.setWord(new Word(wordPractice.getWord().getRu(), wordPractice.getWord().getEn()));
                wp.setCorrect(wordPractice.isCorrect());
                wordPractices.add(wp);
            }
            p.setWordPractices(wordPractices);
            newPractices.add(p);
        }

        return newPractices;
    }

    @Transactional
    public Practice addPractice(List<WordDTO> words, String direction) {
        boolean isEnRu = Objects.equals(direction, "en-ru");
        Practice p = practiceRepository.save(new Practice());
        Practice pAnswer = new Practice();
        pAnswer.setId(p.getId());
        for (WordDTO word : words) {
            Word w;
            WordPractice wp = new WordPractice();
            if (isEnRu) {
                w = wordRepository.findByEn(word.getEn());
                wp.setCorrect(Objects.equals(w.getRu().toLowerCase().trim(), word.getRu().toLowerCase().trim()));
            } else {
                w = wordRepository.findByRu(word.getRu());
                wp.setCorrect(Objects.equals(w.getEn().toLowerCase().trim(), word.getEn().toLowerCase().trim()));
            }
            wp.setWord(w);
            wp.setPractice(p);
            w.getWordPractices().add(wp);
            pAnswer.getWordPractices().add(wp);
        }
        practiceRepository.save(p);
        return pAnswer;
    }

}
