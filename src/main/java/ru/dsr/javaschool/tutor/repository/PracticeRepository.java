package ru.dsr.javaschool.tutor.repository;

import org.springframework.data.repository.CrudRepository;
import ru.dsr.javaschool.tutor.entity.Practice;

public interface PracticeRepository extends CrudRepository<Practice, Long> {

}
