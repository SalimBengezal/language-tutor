package ru.dsr.javaschool.tutor.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.dsr.javaschool.tutor.entity.Word;

import java.util.List;

@Repository

public interface WordRepository extends CrudRepository<Word, Long> {

    Word findByRu(String ru);

    Word findByEn(String en);

    @Query(value = "SELECT ID,EN,RU FROM WORD LEFT JOIN WORD_PRACTICE ON WORD.ID=WORD_PRACTICE.WORD_ID GROUP BY ID,EN,RU ORDER BY COUNT(PRACTICE_ID) LIMIT ?1", nativeQuery = true)
    List<Word> getExerciseWords(Integer limit);
}
