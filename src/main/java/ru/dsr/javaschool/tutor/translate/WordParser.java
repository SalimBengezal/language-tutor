package ru.dsr.javaschool.tutor.translate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordParser {

    private static final List<String> EN_PREPOSITIONS = Arrays.asList("at", "in", "on", "about", "above", "below", "after", "before", "by", "for", "from", "of", "since", "to", "with", "up", "down", "into", "out");
    private static final List<String> EN_ARTICLES = Arrays.asList("a", "an", "the");
    private static final String delimetersRegex = "[^a-zA-Zа-яА-Я]";

    public static List<String> getWords(String text) {
        final String[] words = text.trim().split(delimetersRegex);
        final List<String> wordsList = new ArrayList<>();

        for (String word : words) {
            if (!word.trim().equals("")) {
                if (EN_PREPOSITIONS.contains(word.trim().toLowerCase()))
                    if (wordsList.size() >= 1) {
                        if (isEnglish(word.trim()) && !EN_PREPOSITIONS.contains(wordsList.get(wordsList.size() - 1))) {
                            String newWord = wordsList.get(wordsList.size() - 1) + " " + word.trim();
                            if (!wordsList.contains(newWord.toLowerCase()))
                                wordsList.add(newWord.toLowerCase());
                        }
                    }
                if (!wordsList.contains(word.trim().toLowerCase()))
                    wordsList.add(word.trim().toLowerCase());
            }
        }
        wordsList.removeIf(EN_ARTICLES::contains);

        return wordsList;
    }

    private static boolean isRussian(String word) {
        boolean isRus = true;
        for (char ch :
                word.toCharArray()) {
            if (Character.toLowerCase(ch) < 'а' || Character.toLowerCase(ch) > 'я')
                isRus = false;
        }
        return isRus;
    }

    private static boolean isEnglish(String word) {
        boolean isEng = true;
        for (char ch :
                word.toCharArray()) {
            if (Character.toLowerCase(ch) < 'a' || Character.toLowerCase(ch) > 'z')
                isEng = false;
        }
        return isEng;
    }

}
