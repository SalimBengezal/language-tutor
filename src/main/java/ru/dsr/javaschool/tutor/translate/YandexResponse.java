package ru.dsr.javaschool.tutor.translate;

public class YandexResponse {
    private int code;
    private String lang;
    private String[] text;

    public int getCode() {
        return code;
    }

    public String getLang() {
        return lang;
    }

    public String[] getText() {
        return text;
    }
}
