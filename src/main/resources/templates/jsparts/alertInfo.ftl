<script>

    function alertInfo(header, text, status) {
        $("#alertInfoDiv").empty();
        $('#alertInfoDiv').append(
                '<div class="alert alert-' + status + '" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '<strong> ' + header + ' </strong> ' + text + ' </div>'
        );
    }

    function alertValidation(messages) {
        $("#alertInfoDiv").empty();

        for (var i = 0, message; message = messages[i]; i++) {
            $('#alertInfoDiv').append(
                    '<div class="alert alert-warning" role="alert">' +
                    '<strong> Внимание! </strong> Поле "<b>' + message.fieldName + '</b>" : ' + message.msg +
                    '</div>'
            );
        }
    }

</script>

<div id="alertValidationDiv"></div>
<div id="alertInfoDiv"></div>
