<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Language Tutor</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="/assets/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">

<#include "navbar.ftl">