<nav class="navbar navbar-default navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Tutor</a>
        </div>
        <ul class="nav navbar-nav">
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                   aria-expanded="false">
                    Добавление слов <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/add/word">Одного слова</a></li>
                    <li><a href="/add/text">Несколько слов из текста</a></li>
                </ul>
            </li>
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                   aria-expanded="false">
                    Тестирование <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/practice/quickstart">Быстрый старт</a></li>
                    <li><a href="/practice/choice">Составить свой список слов для проверки</a></li>
                    <li><a href="/practice/statistics">Статистика</a></li>
                </ul>
            </li>
            <li><a href="/words">База знаний</a></li>
        </ul>
    </div>
</nav>