<script>
    $(document).ready(
            function () {
                getWords();
            });

    function getWords() {
        $.ajax(
                {
                    dataType: 'json',
                    type: 'GET',
                    url: '/api/word',
                    success: function getData(data) {
                        $('#words').empty();
                        $('#words').append('<thead><tr><th>Слова на английском языке</th><th>Слова на русском языке</th></tr></thead><tbody>');
                        for (var i = 0; i < data.length; i++) {
                            $('#words').append('<tr>'
                                    + '<td>' + data[i].en + '</td>'
                                    + '<td>' + data[i].ru + '</td>'
                            )
                        }
                        $('#words').append('</tbody>');
                    }
                    ,
                    error: function () {
                        alert("Возникла ошибка");
                    }
                }
        )
    }
</script>

<div class="container-fluid">
    <h3>База знаний</h3>
    <table id="words" class="table table-responsive"></table>
</div>