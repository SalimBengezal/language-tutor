<#-- @ftlroot ".." -->
<#include "/layout/header.ftl">

<script>

    function choiceDirection(direction) {
        $("#startButtons").hide();
        $("#wordsChoiceDiv").show();

        if (direction == "en-ru") {
            document.getElementById("sendBtn").setAttribute('onclick', 'sendCheck("en-ru")');
            document.getElementById("addWordsBtn").setAttribute('onclick', 'startTest("en-ru")');
        }
        else if (direction == 'ru-en') {
            document.getElementById("sendBtn").setAttribute('onclick', 'sendCheck("ru-en")');
            document.getElementById("addWordsBtn").setAttribute('onclick', 'startTest("ru-en")');
        }
        $.ajax(
                {
                    dataType: 'json',
                    type: 'GET',
                    url: '/api/word/get/' + direction,
                    success: function getData(words) {
                        fillTable(words, direction);
                    }
                    ,
                    error: function(response) {
                        var jsonAnswer = jQuery.parseJSON(response.responseText);
                        alertValidation(jsonAnswer);
                    }
                }
        );
        alertInfo("Создайте свой список слов!", "Выберите слова для тестирования", "info");
    }

    function fillTable(words, direction) {
        $('#wordsList').empty();
        if (direction == 'en-ru') {
            $('#wordsList').append('<thead><tr><th>Слова на английском языке</th><th>Тестировать</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsList').append('<tr>'
                        + '<td>' + words[i].en + '</td>'
                        + '<td>' + '<input type=checkbox class="form-control text-center"></td>'
                )
            }
        }
        else if (direction == 'ru-en') {
            $('#wordsList').append('<thead><tr><th>Слова на русском языке</th><th>Тестировать</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsList').append('<tr>'
                        + '<td>' + words[i].ru + '</td>'
                        + '<td>' + '<input type=checkbox class="form-control text-center"></td>'
                )
            }
        }
        $('#wordsList').append('</tbody>');
    }

    function startTest(direction) {
        $("#wordsChoiceDiv").hide();
        $("#WordsToCheck").show();
        alertInfo("Тестирование начато!", "", "info");

        var table = document.getElementById("wordsList");
        var words = [];
        for (var i = 1, row; row = table.rows[i]; i++) {
            var word;
            if (direction == 'en-ru')
                word = {
                    en: row.cells[0].innerText,
                    ru: ""
                };
            else if (direction == 'ru-en')
                word = {
                    en: "",
                    ru: row.cells[0].innerText
                };
            if (row.cells[1].getElementsByTagName("input")[0].checked)
                words.push(word);
        }

        fillWordsToTableTest(words, direction);
    }

    function fillWordsToTableTest(words, direction) {
        $('#wordsTable').empty();
        if (direction == 'en-ru') {
            $('#wordsTable').append('<thead><tr><th>Слово на английском языке</th><th>Слова на русском языке</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsTable').append('<tr>'
                        + '<td>' + '<input readonly tabindex="-1" type=text class="form-control enWord" value="' + words[i].en + '"></td>'
                        + '<td>' + '<input type=text class="form-control ruWord" value="' + words[i].ru + '"></td>'
                )
            }
        }
        else if (direction == 'ru-en') {
            $('#wordsTable').append('<thead><tr><th>Слово на русском языке</th><th>Слова на английском языке</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsTable').append('<tr>'
                        + '<td>' + '<input readonly tabindex="-1" type=text class="form-control ruWord" value="' + words[i].ru + '"></td>'
                        + '<td>' + '<input type=text class="form-control enWord" value="' + words[i].en + '"></td>'
                )
            }
        }
        $('#wordsTable').append('</tbody>');

    }

    function sendCheck(direction) {
        $("#resultsDiv").show();
        $("#WordsToCheck").hide();
        alertInfo("Проверка завершена!", "", "success");

        var table = document.getElementById("wordsTable");
        var words = [];
        for (var i = 1, row; row = table.rows[i]; i++) {
            var word = {
                en: row.getElementsByClassName('enWord')[0].value,
                ru: row.getElementsByClassName('ruWord')[0].value
            };
            words.push(word);
        }

        $.ajax({
            dataType: "json",
            type: "POST",
            data: JSON.stringify(words),
            contentType: "application/json",
            url: "/api/practice/check/" + direction,
            success: function getData(practice) {
                showResults(practice, direction);
            },
            error: function(response) {
                var jsonAnswer = jQuery.parseJSON(response.responseText);
                alertValidation(jsonAnswer);
            }
        });
    }

    function showResults(practice, direction) {

        $("#resultsTable").empty();
        wordPractices = practice.wordPractices;
        if (direction == 'en-ru') {
            $('#resultsTable').append('<thead><tr><th>Слово на английском языке</th><th>Слова на русском языке</th></tr></thead><tbody>');
            for (var i = 0, wordPractice; wordPractice = wordPractices[i]; i++) {
                var status = 'danger';
                if (wordPractice.correct)
                    status = 'success';
                $('#resultsTable').append('<tr class="' + status + '">'
                        + '<td>' + '<input readonly type=text class="form-control enWord" value="' + wordPractice.word.en + '"></td>'
                        + '<td>' + '<input type=text class="form-control ruWord" value="' + wordPractice.word.ru + '"></td>'
                )
            }
        }
        else if (direction == 'ru-en') {
            $('#resultsTable').append('<thead><tr><th>Слово на русском языке</th><th>Слова на английском языке</th></tr></thead><tbody>');
            for (var i = 0, wordPractice; wordPractice = wordPractices[i]; i++) {
                var status = 'danger';
                if (wordPractice.correct)
                    status = 'success';
                $('#resultsTable').append('<tr class="' + status + '">'
                        + '<td>' + '<input readonly type=text class="form-control ruWord" value="' + wordPractice.word.en + '"></td>'
                        + '<td>' + '<input type=text class="form-control enWord" value="' + wordPractice.word.ru + '"></td>'
                )
            }
        }
        $('#resultsTable').append('</tbody>');
    }

</script>


<div class="container-fluid">
<#include "/jsparts/alertInfo.ftl">

    <h3>Тестирование</h3>

    <div class="row text-center" id="startButtons">
        <div class="col-sm-6">
            <button class="btn btn-info btn-lg" onclick="choiceDirection('en-ru')">Тестирование (EN -> RU)</button>
        </div>
        <div class="col-sm-6">
            <button class="btn btn-info btn-lg" onclick="choiceDirection('ru-en')">Тестирование (RU -> EN)</button>
        </div>
    </div>

    <div id="wordsChoiceDiv" style="display: none">
        <table id="wordsList" class="table"></table>
        <div class="text-right">
            <button id="addWordsBtn" class="btn btn-primary">Начать тестирование</button>
        </div>
    </div>

    <div id="WordsToCheck" style="display: none">
        <table id="wordsTable" class="table"></table>
        <div class="text-right">
            <button id="sendBtn" class="btn btn-primary">Проверить</button>
        </div>
    </div>

    <div id="resultsDiv" style="display: none">
        <h4>Результаты</h4>
        <table id="resultsTable" class="table"></table>
    </div>

</div>

<#include "/layout/footer.ftl">