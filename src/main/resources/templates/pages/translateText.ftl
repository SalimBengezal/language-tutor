<#-- @ftlroot ".." -->
<#include "/layout/header.ftl">

<script>
    function translateWords() {
        var radioBtn = document.getElementsByName("direction");
        if (radioBtn[0].checked)
            direction = "en-ru";
        else if (radioBtn[1].checked)
            direction = "ru-en";
        var text = $("#text").val();
        if (text.trim() == "") {
            alertInfo("Внимание!", "Текст не может быть пустым", "warning");
            return;
        }
        $.ajax({
            dataType: "json",
            type: "POST",
            data: text,
            contentType: "application/json",
            url: "/api/word/" + direction + "/translate",
            success: function getData(words) {
                $("#addDiv").show();
                fillTable(words, direction);
                alertInfo("Перевод завершен!", "Проверьте правильность слов и измените, если необходимо", "success");
            },
            error: function (response) {
                var jsonAnswer = jQuery.parseJSON(response.responseText);
                alertValidation(jsonAnswer);
            }
        });
    }

    function fillTable(words, direction) {
        $('#wordsTable').empty();
        if (direction == 'en-ru') {
            $('#wordsTable').append('<thead><tr><th>Слова на английском языке</th><th>Слова на русском языке</th><th>Добавить</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsTable').append('<tr>'
                        + '<td>' + '<input type=text class="form-control enWord" value="' + words[i].en + '"></td>'
                        + '<td>' + '<input type=text class="form-control ruWord" value="' + words[i].ru + '"></td>'
                        + '<td>' + '<input type=checkbox class="form-control text-center" checked></td>'
                )
            }
        }
        else if (direction == 'ru-en') {
            $('#wordsTable').append('<thead><tr><th>Слова на русском языке</th><th>Слова на английском языке</th><th>Добавить</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsTable').append('<tr>'
                        + '<td>' + '<input type=text class="form-control ruWord" value="' + words[i].ru + '"></td>'
                        + '<td>' + '<input type=text class="form-control enWord" value="' + words[i].en + '"></td>'
                        + '<td>' + '<input type=checkbox class="form-control text-center" checked></td>'
                )
            }
        }
        $('#wordsTable').append('</tbody>');
    }

    function addWords() {
        var table = document.getElementById("wordsTable");
        var words = [];
        for (var i = 1, row; row = table.rows[i]; i++) {
            var word = {
                en: row.getElementsByClassName('enWord')[0].value,
                ru: row.getElementsByClassName('ruWord')[0].value
            };
            if (row.cells[2].getElementsByTagName('input')[0].checked)
                words.push(word);
        }

        $.ajax({
            dataType: "json",
            type: "POST",
            data: JSON.stringify(words),
            contentType: "application/json",
            url: "/api/word/add/list",
            success: function () {
                alertInfo("Cлова добавлены!", "", "success");
            },
            error: function (response) {
                var jsonAnswer = jQuery.parseJSON(response.responseText);
                for (var i = 0, message; message = jsonAnswer[i]; i++) {
                    var fieldText = message.fieldName;
                    var begin = fieldText.indexOf("[");
                    var end = fieldText.indexOf("]");
                    var num = fieldText.substr(begin+1, end-begin-1);
                    var dot = fieldText.indexOf(".");
                    var lang = fieldText.substr(dot+1,2);
                    message.fieldName = lang + " №" + (Number(num)+1);
                }
                alertValidation(jsonAnswer);
            }
        });

    }

    function addAll() {
        var table = document.getElementById("wordsTable");
        for (var i = 1, row; row = table.rows[i]; i++) {
            row.cells[2].getElementsByTagName('input')[0].checked = true;
        }
    }

    function clearSelection() {
        var table = document.getElementById("wordsTable");
        for (var i = 1, row; row = table.rows[i]; i++) {
            row.cells[2].getElementsByTagName('input')[0].checked = false;
        }
    }

</script>


<div class="container-fluid">
<#include "/jsparts/alertInfo.ftl">
    <div class="row">
        <div class="col-md-7"><h3>Добавление слов из текста</h3></div>
        <div class="col-md-5 bottom-right">
            <div class="form-group">
                <p>Направление перевода
                    <label class="checkbox-inline">
                        <input type="radio" name="direction" value="en-ru" checked>
                    </label> EN->RU
                    <label class="checkbox-inline">
                        <input type="radio" name="direction" value="ru-en">
                    </label> RU->EN
                </p>
            </div>
        </div>
    </div>
    <div class="form-group">
        <textarea id="text" rows="5" class="form-control" placeholder="Введите слова либо текст"></textarea>
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary" onclick="translateWords()">Перевести слова</button>
    </div>
</div>

<hr>

<div class="container-fluid" id="addDiv" style="display: none">
    <div class="row">
        <div class="col-sm-9">
            <h3>Добавление слов</h3>
        </div>
        <div class="col-sm-3">
            <a style="cursor: pointer" onclick="addAll()">Отметить все</a> /
            <a style="cursor: pointer" onclick="clearSelection()">Убрать все</a>
        </div>
    </div>
    <table id="wordsTable" class="table table-responsive"></table>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary" onclick="addWords()">Добавить</button>
    </div>
</div>

<#include "/layout/footer.ftl">