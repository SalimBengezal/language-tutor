<#-- @ftlroot ".." -->
<#include "/layout/header.ftl">

<script>

    function startTest(direction) {
        $("#startButtons").hide();
        $("#WordsToCheck").show();
        alertInfo("Тестирование начато!", "Вводите слова на альтернативном языке в поля", "info");

        if (direction == "en-ru") {
            document.getElementById("sendBtn").setAttribute('onclick', 'sendCheck("en-ru")');
        }
        else if (direction == 'ru-en') {
            document.getElementById("sendBtn").setAttribute('onclick', 'sendCheck("ru-en")');
        }

        $.ajax(
                {
                    dataType: 'json',
                    type: 'GET',
                    url: '/api/practice/quickstart/' + direction,
                    success: function getData(words) {
                        fillTable(words, direction);
                    }
                    ,
                    error: function(response) {
                        var jsonAnswer = jQuery.parseJSON(response.responseText);
                        alertValidation(jsonAnswer);
                    }
                }
        )
    }

    function fillTable(words, direction) {
        $('#wordsTable').empty();
        if (direction == 'en-ru') {
            $('#wordsTable').append('<thead><tr><th>Слово на английском языке</th><th>Слова на русском языке</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsTable').append('<tr>'
                        + '<td>' + '<input tabindex="-1" readonly type=text class="form-control enWord" value="' + words[i].en + '"></td>'
                        + '<td>' + '<input type=text class="form-control ruWord" value="' + words[i].ru + '"></td>'
                )
            }
        }
        else if (direction == 'ru-en') {
            $('#wordsTable').append('<thead><tr><th>Слово на русском языке</th><th>Слова на английском языке</th></tr></thead><tbody>');
            for (var i = 0; i < words.length; i++) {
                $('#wordsTable').append('<tr>'
                        + '<td>' + '<input readonly tabindex="-1" type=text class="form-control ruWord" value="' + words[i].ru + '"></td>'
                        + '<td>' + '<input type=text class="form-control enWord" value="' + words[i].en + '"></td>'
                )
            }
        }
        $('#wordsTable').append('</tbody>');

    }

    function sendCheck(direction) {
        var table = document.getElementById("wordsTable");
        var words = [];
        for (var i = 1, row; row = table.rows[i]; i++) {
            var word = {
                en: row.getElementsByClassName('enWord')[0].value,
                ru: row.getElementsByClassName('ruWord')[0].value
            };
            words.push(word);
        }

        $.ajax({
            dataType: "json",
            type: "POST",
            data: JSON.stringify(words),
            contentType: "application/json",
            url: "/api/practice/check/" + direction,
            success: function getData(practice) {
                showResults(practice, direction);
            },
            error: function(response) {
                var jsonAnswer = jQuery.parseJSON(response.responseText);
                alertValidation(jsonAnswer);
            }
        });
    }

    function showResults(practice, direction) {
        $("#WordsToCheck").hide();
        $("#resultsDiv").show();

        $('#resultsTable').empty();
        alertInfo("Проверка завершена!", "", "success");
        wordPractices = practice.wordPractices;
        if (direction == 'en-ru') {
            $('#resultsTable').append('<thead><tr><th>Слово на английском языке</th><th>Слова на русском языке</th></tr></thead><tbody>');
            for (var i = 0, wordPractice; wordPractice = wordPractices[i]; i++) {
                var status = 'danger';
                if (wordPractice.correct)
                    status = 'success';
                $('#resultsTable').append('<tr class="' + status + '">'
                        + '<td>' + '<input readonly type=text class="form-control enWord" value="' + wordPractice.word.en + '"></td>'
                        + '<td>' + '<input type=text class="form-control ruWord" value="' + wordPractice.word.ru + '"></td>'
                )
            }
        }
        else if (direction == 'ru-en') {
            $('#resultsTable').append('<thead><tr><th>Слово на русском языке</th><th>Слова на английском языке</th></tr></thead><tbody>');
            for (var i = 0, wordPractice; wordPractice = wordPractices[i]; i++) {
                var status = 'danger';
                if (wordPractice.correct)
                    status = 'success';
                $('#resultsTable').append('<tr class="' + status + '">'
                        + '<td>' + '<input readonly type=text class="form-control ruWord" value="' + wordPractice.word.en + '"></td>'
                        + '<td>' + '<input type=text class="form-control enWord" value="' + wordPractice.word.ru + '"></td>'
                )
            }
        }
        $('#wordsTable').append('</tbody>');

    }

</script>

<div class="container-fluid">
<#include "/jsparts/alertInfo.ftl">

    <h3>Тестирование</h3>

    <div class="row text-center" id="startButtons">
        <div class="col-sm-6">
            <button class="btn btn-info btn-lg" onclick="startTest('en-ru')">Начать тестирование (EN -> RU)</button>
        </div>
        <div class="col-sm-6">
            <button class="btn btn-info btn-lg" onclick="startTest('ru-en')">Начать тестирование (RU -> EN)</button>
        </div>
    </div>

    <div id="WordsToCheck" style="display: none">
        <table id="wordsTable" class="table"></table>
        <div class="text-right">
            <button id="sendBtn" class="btn btn-primary">Проверить</button>
        </div>
    </div>

    <div id="resultsDiv" style="display: none">
        <h4>Результаты</h4>
        <table id="resultsTable" class="table"></table>
    </div>

</div>

<#include "/layout/footer.ftl">