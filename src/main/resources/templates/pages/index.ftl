<#-- @ftlroot ".." -->
<#include "/layout/header.ftl">

<div class="container">
    <h2>Репетитор.Online</h2>
    <div class="container">
        <div class="col-sm-6">
            <h4>На данном сайте вы можете:</h4>
            <ul>
                <li>Проверить свой словарный запас</li>
                <li>Мониторить свой прогресс</li>
                <li>Пополнять базу знаний своими словами</li>
                <li>Получать онлайн-перевод слов из текста</li>
                <li>Полезно проводить свое время</li>
            </ul>
        </div>
        <div class="col-sm-6">
            <div class="jumbotron" style="background-color: transparent">
                <a href="/practice/quickstart" class="btn btn-info btn-lg">Начать тренировку</a>
            </div>
        </div>
    </div>
</div>


<#include "/layout/footer.ftl">