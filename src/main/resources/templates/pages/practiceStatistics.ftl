<#-- @ftlroot ".." -->
<#include "/layout/header.ftl">

<script>
    $(document).ready(
            function () {
                getStatistics();
            });

    function getStatistics() {
        $.ajax(
                {
                    dataType: 'json',
                    type: 'GET',
                    url: '/api/practice/statistics',
                    success: function getData(data) {
                        $('#practices').empty();
                        for (var i = 0; i < data.length; i++) {
                            addPracticeToTable(data[i])
                        }
                    }
                    ,
                    error: function(response) {
                        var jsonAnswer = jQuery.parseJSON(response.responseText);
                        alertValidation(jsonAnswer);
                    }
                }
        )
    }

    function addPracticeToTable(practice) {
        $('#practices').append('<div class="container-fluid"><h4>Тест №' + practice.id + '   <small id="stat-' + practice.id + '"></small></h4>' +
                '<div class="progress">' +
                '<div aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-success " id="progress-right-' + practice.id + '"></div>' +
                '<div aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-danger" id="progress-wrong-' + practice.id + '"></div>' +
                '</div>' +
                '<div id="words-' + practice.id + '"></div></div>');
        wordPractices = practice.wordPractices;
        var all = 0;
        var right = 0;
        var wordDiv = document.getElementById('words-' + practice.id);
        for (var i = 0, wordPractice; wordPractice = wordPractices[i]; i++) {
            var w = wordPractice.word;
            var status='bg-danger';
            all++;
            if (wordPractice.correct) {
                status = 'bg-success';
                right += 1;
            }
            wordDiv.innerHTML +=
                    '<div style="margin-bottom: 15px;"class="col-lg-2 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-2 panel-body img-rounded ' + status + '">' +
                    '<div class="input-group form-group">' +
                    '<span class="input-group-addon">EN</span>' +
                    '<input type="text" class="form-control" readonly value="' + w.en + '">' +
                    '</div>' +
                    '<div class="input-group">' +
                    '<span class="input-group-addon">RU</span>' +
                    '<input type="text" class="form-control" readonly value="' + w.ru + '">' +
                    '</div>' +
                    '</div>';
        }
        $("#progress-right-" + practice.id).width((right / all) * 100 + '%');
        $("#progress-wrong-" + practice.id).width((1 - right / all) * 100 + '%');
        $("#stat-" + practice.id).append("Всего: " + all + " cлов | Верно: " + right + " | Неверно: " + (all - right));
    }
</script>

<style>

</style>

<div class="container-fluid">
    <h3>Статистика по тестам</h3>
    <div id="practices"></div>
</div>

<#include "/layout/footer.ftl">