<#-- @ftlroot ".." -->
<#include "/layout/header.ftl">

<script>
    function addWord() {

        var enField = $('#en');
        var ruField = $('#ru');
        var word = {
            en: enField.val(),
            ru: ruField.val()
        };
        $.ajax({
            dataType: "json",
            type: "POST",
            data: JSON.stringify(word),
            contentType: "application/json",
            url: "/api/word/add",
            success: function () {
                getWords();
                alertInfo("Слово добавлено!", "EN: " + enField.val() + ". RU: " + ruField.val(), 'success');
                enField.val("");
                ruField.val("");

            },
            error: function(response) {
                var jsonAnswer = jQuery.parseJSON(response.responseText);
                alertValidation(jsonAnswer);
            }
        });
    }

</script>

<div class="container-fluid">
<#include "/jsparts/alertInfo.ftl">
    <h3>Добавление слова</h3>
    <div class="container-fluid">
        <div class="col-sm-5">
            <div class="input-group">
                <span class="input-group-addon">EN</span>
                <input type="text" class="form-control" id="en" placeholder="Слово на английском языке">
            </div>
        </div>
        <div class="col-sm-5">
            <div class="input-group">
                <span class="input-group-addon">RU</span>
                <input type="text" class="form-control" id="ru" placeholder="Слово на русском языке">
            </div>
        </div>
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary" onclick="addWord()">Добавить</button>
        </div>
    </div>
</div>

<hr>

<#include "/word/wordsTable.ftl">

<#include "/layout/footer.ftl">